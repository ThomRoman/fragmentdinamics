package com.android.fragmentdinamic;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {

    FragmentTransaction transaction;
    Fragment home,rojo,verde;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        home = new HomeFragment();
        verde = new Fragment1();
        rojo = new Fragment2();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contenedorFragments,home)
                .commit();

    }

    public void onClick(View view){
        transaction = getSupportFragmentManager().beginTransaction();
        switch (view.getId()){
            case R.id.btnRojo:
                transaction
                        .replace(R.id.contenedorFragments,rojo)
                        .addToBackStack(null);
                break;
            case R.id.btnVerde:
                transaction
                        .replace(R.id.contenedorFragments,verde)
                        .addToBackStack(null);
                break;
        }
        transaction.commit();

    }
}
//https://material.io/components/bottom-navigation/android#bottom-navigation-bar